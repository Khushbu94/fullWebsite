/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(1);

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = "<!DOCTYPE html>\n<html>\n\n<head>\n  <title>HTML-3</title>\n  <meta charset=\"UTF-8\">\n  <link rel=\"stylesheet\" href=\"node_modules/bootstrap/dist/css/bootstrap.min.css\">\n  <script src=\"node_modules/jquery/dist/jquery.js\"></script>\n  <script src=\"node_modules/tether/dist/js/tether.min.js\"></script>\n  <script src=\"node_modules/bootstrap/dist/js/bootstrap.min.js\"></script>\n\n  <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">\n  <link rel=\"stylesheet\" type=\"text/css\" href=\"./assets/css/style.css\">\n</head>\n\n<body>\n\n  <div class=\"container\">\n    <div class=\"row \">\n      <div class=\"col-md-12\">\n        <nav class=\"nav nav-color\">\n          <a class=\"nav-link nav-color \" href=\"#\">SINGOLO</a>\n          <a class=\"nav-link nav-color floating\" href=\"#\">HOME</a>\n          <a class=\"nav-link nav-color\" href=\"#\">SERVICES</a>\n          <a class=\"nav-link nav-color\" href=\"#\">PORTFOLIO</a>\n        </nav>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-md-12\">\n\n        <div id=\"carouselExampleControls\" class=\"carousel slide\" data-ride=\"carousel\">\n          <div class=\"carousel-inner\" role=\"listbox\">\n            <div class=\"carousel-item active sizemyimage\">\n\n              <img class=\"d-block img-fluid sizemyimage verticalimage\" src=\"/assets/images/verticalphone.png\" alt=\"First slide\">\n              <img class=\"d-block img-fluid sizemyimage horizontalimage\" src=\"/assets/images/horizontalphone.png\" alt=\"First slide\">\n            </div>\n            <div class=\"carousel-item sizemyimage\">\n              <img class=\"d-block img-fluid sizemyimage verticalimage\" src=\"/assets/images/verticalphone.png\" alt=\"First slide\">\n              <img class=\"d-block img-fluid sizemyimage horizontalimage\" src=\"/assets/images/horizontalphone.png\" alt=\"First slide\">\n            </div>\n            <div class=\"carousel-item sizemyimage\">\n              <img class=\"d-block img-fluid sizemyimage verticalimage\" src=\"/assets/images/verticalphone.png\" alt=\"First slide\">\n              <img class=\"d-block img-fluid sizemyimage horizontalimage\" src=\"/assets/images/horizontalphone.png\" alt=\"First slide\">\n            </div>\n          </div>\n          <a class=\"carousel-control-prev\" href=\"#carouselExampleControls\" role=\"button\" data-slide=\"prev\">\n            <span class=\"carousel-control-prev-icon \" aria-hidden=\"true\"></span>\n            <span class=\"sr-only\">Previous</span>\n          </a>\n          <a class=\"carousel-control-next \" href=\"#carouselExampleControls\" role=\"button\" data-slide=\"next\">\n            <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>\n            <span class=\"sr-only\">Next</span>\n          </a>\n        </div>\n      </div>\n\n    </div>\n    <div class=\"row\">\n      <div class=\"col-md-12\">\n        <h1>Our Services</h1>\n      </div>\n    </div>\n\n\n\n\n\n\n\n\n\n\n\n\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n    <script type=\"text/javascript\" src=\"./dist/bundle.js\"></script>\n<script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script><script type=\"text/javascript\" src=\"./dist/bundle.js\"></script></body>\n\n</html>"

/***/ })
/******/ ]);